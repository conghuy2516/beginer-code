#include<iostream>
#include<cstring>
using namespace std;
int val[]={1,5,10,50,100,500,1000};
char s[]="IVXLCDM";
int find_Pos(char c)
{
    for(int i=0;i<7;i++)
    {
        if(s[i]==c) return i;
    }
}
int  solve(char c[])
{
    int n=strlen(c);
    int res=val[find_Pos(c[n-1])];
    for(int i=n-1;i>0;i--)
    {
        int pos1=find_Pos(c[i]);
        int pos2=find_Pos(c[i-1]);
        if(val[pos1]<=val[pos2])
        {
            res+=val[pos2];
        }
        else res-=val[pos2];
    }
    return res;
}
int main()
{
    int t;
    cin>>t;
    getchar();
    while(t--)
    {
        char c[1005];
        cin.getline(c,1004);
        cout<<solve(c);
        cout<<endl;
    }
    return 0;
}
